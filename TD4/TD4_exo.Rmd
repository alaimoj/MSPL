---
title: "TD4_exo"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## Loading INSEE data
```{r}
df <- read.csv("data/dpt2016.txt", sep = "\t");
head(df);
```

```{r}
library(ggplot2);
```

Let's plot the simplest case, just points and a line interpolating them.

```{r}
ggplot(data=df, aes(x=preusuel, y=annais)) + geom_point() + geom_line();
```

```{r}
str(df);
```


```{r}
ggplot(data=df) + geom_bar(aes(x=annais))  + scale_x_discrete(limit = c(19))
```

 geom_histogram()

```{r}
ggplot(subset(df), aes(annais,preusuel)) +
      geom_jitter();
```
